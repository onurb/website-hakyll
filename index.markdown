---
title: FMBC 2020
---

# 2nd Workshop on Formal Methods for Blockchains

July 19, 2020, Los Angeles, USA

Co-located with the 32nd International Conference on Computer-Aided Verification [(CAV 2020)](http://i-cav.org/2020/)


## Overview

Blockchains are decentralised transactional ledgers that rely on
cryptographic hash functions for guaranteeing the integrity of the
stored data. Participants on the network reach agreement on what valid
transactions are through consensus algorithms.

Blockchains may also provide support for Smart Contracts. Smart
Contracts are scripts of an ad-hoc programming language that are
stored in the blockchain and that run on the network. They can
interact with the ledger’s data and update its state. These scripts
can express the logic of possibly complex contracts between users of
the blockchain. Thus, Smart Contracts can facilitate the economic
activity of blockchain participants.

With the emergence and increasing popularity of cryptocurrencies such
as Bitcoin and Ethereum, it is now of utmost importance to have strong
guarantees of the behaviour of blockchain software. These guarantees
can be brought by using Formal Methods. Indeed, Blockchain software
encompasses many topics of computer science where using Formal Methods
techniques and tools is relevant: consensus algorithms to ensure the
liveness and the security of the data on the chain, programming
languages specifically designed to write smart contracts,
cryptographic protocols, such as zero-knowledge proofs, used to ensure
privacy, etc.

### Scope and Topics
This workshop is a forum to identify theoretical and practical
approaches of formal methods for blockchain technology. Topics
include, but are not limited to:

- Formal models of blockchain applications or concepts
- Formal methods for consensus protocols
- Formal methods for blockchain-specific cryptographic primitives or protocols
- Formal languages for Smart Contracts
- Verification of Smart Contracts

## Call for Papers

### Paper Submissions

We will consider original manuscripts (not published or considered
elsewhere) with a maximum (including references and appendices) of
**twelve pages** (regular papers), **six pages** (short papers), and **two pages**
(extended abstract) describing new and emerging ideas or summarizing
existing work). Each paper should include a title and the name and
affiliation of each author. Authors of selected extended-abstracts are
invited to give a short lightning talk of up to 15 minutes. At least
one author of an accepted paper is expected to present the paper at
the workshop as a registered participant. All accepted contributions
will be reviewed once more by the program committee after the workshop
and before being included in the post-proceedings.


(Note: this is a draft, suggestions: say 1st three kinds of
permissions , and abstracts could be 2-3 pages long.)

### Proceedings
 TBA

## Important Dates

TBA

## [Invited Speaker](#is)

[Grigore Rosu](http://fsl.cs.illinois.edu/index.php/Grigore_Rosu), University of Illinois at Urbana-Champaign, USA


## Program Committee

## Organisers

## Previous Edition

- [FMBC 2019](https://sites.google.com/view/fmbc) (Porto, Portugal), co-located with [FM'19](http://formalmethods2019.inesctec.pt/)
